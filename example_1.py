class Dog:
    kind = "familiar"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name} is {self.age} years old"

    def speak(self, sound):
        return f"{self.name} says {sound}"



dog_1 = Dog("Tuzik", 5)
dog_2 = Dog("Scharik", 3)
dog_3 = Dog("Dexter", 7)

print(dog_1)

print(str(dog_1))




