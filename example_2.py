"""
https://docs.python.org/3/reference/datamodel.html
"""

class Basket:
    cart = []
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"it is {self.name}`s basket"

    def __add__(self, other):
        self.cart.append(other)

    def __len__(self):
        return len(self.cart)



"""len() -> __len__
+ --> __add__"""

basket_1 = Basket("Ivan")
print(basket_1)

basket_1 + "socks"

basket_1 + "beer"

print(basket_1.cart)

print(len(basket_1))

len(basket_1) == basket_1.__len__()
bool(basket_1)

